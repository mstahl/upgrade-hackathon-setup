#!/usr/bin/env python
from __future__ import print_function

URL = ('https://cern.ch/lhcb-nightlies-artifacts/'
       '{flavour}/{slot}/{id}/slot-config.json')


def get_config3(slot, id='Today', flavour='nightly'):
    '''
    Get nightly build slot configuration.
    '''
    from urllib.request import urlopen
    from json import loads
    return loads(
        str(
            urlopen(URL.format(flavour=flavour, slot=slot, id=id)).read(),
            'utf-8'))


def get_config2(slot, id='Today', flavour='nightly'):
    '''
    Get nightly build slot configuration.
    '''
    from urllib2 import urlopen
    from json import load
    return load(urlopen(URL.format(flavour=flavour, slot=slot, id=id)))


import sys
if sys.version_info[0] == 2:
    get_config = get_config2
else:
    get_config = get_config3

PROJECT_SNIPPET = '''
{name}_URL=https://gitlab.cern.ch/lhcb-nightlies/{name}.git
{name}_BRANCH={prefix}{slot}/{build_id}'''


def main():
    from argparse import ArgumentParser
    parser = ArgumentParser()

    parser.add_argument('-f', '--flavour', action='store', default='nightly')

    parser.add_argument(
        'slot',
        help='slot name with optional build id '
        '(e.g. lhcb-head/Tue)')

    args = parser.parse_args()

    if '/' in args.slot:
        args.slot, args.id = args.slot.split('/', 1)
    else:
        args.id = 'Today'

    config = get_config(slot=args.slot, id=args.id, flavour=args.flavour)

    names = set(
        proj['name'] for proj in config['projects']
        if not proj.get('disabled') and proj['name'] not in ('DBASE', 'PARAM'))
    for proj in config['projects']:
        if proj.get('disabled') or proj['name'] in ('DBASE', 'PARAM'):
            continue
        print(PROJECT_SNIPPET.format(
            name=proj['name'],
            prefix=(args.flavour + '/') if args.flavour != 'nightly' else '',
            slot=config['slot'],
            build_id=config['build_id']))
    print('\nPROJECTS={}'.format(' '.join(sorted(names))))


if __name__ == '__main__':
    main()
